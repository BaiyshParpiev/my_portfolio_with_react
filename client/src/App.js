import GlobalStyle from "./style";
import {ThemeProvider} from "styled-components";
import {lightTheme} from "./components/Themes/Themes";
import {Route, Switch, useLocation} from "react-router-dom";
import Main from "./components/Main/Main";
import WorkPage from "./components/WorkPage/WorkPage";
import AboutPage from "./components/AboutPage/AboutPage";
import SkillsPage from "./components/SkillsPage/SkillsPage";
import {AnimatePresence} from "framer-motion";

function App() {
    const location = useLocation();
  return <>
      <GlobalStyle/>
      <ThemeProvider theme={lightTheme}>
          <AnimatePresence exitBeforeEnter>
              <Switch location={location} key={location.pathname}>
                  <Route path="/" exact component={Main}/>
                  <Route path="/works" component={WorkPage}/>
                  <Route path="/skills" component={SkillsPage}/>
                  <Route path="/about" component={AboutPage}/>
              </Switch>
          </AnimatePresence>
      </ThemeProvider>
    </>
    
}

export default App

