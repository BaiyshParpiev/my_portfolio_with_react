import React from 'react';
import styled, {keyframes, ThemeProvider} from "styled-components";
import {darkTheme} from "../Themes/Themes";
import LogoComponent from "../../subComponents/UI/LogoComponent/LogoComponent";
import PowerButton from "../../subComponents/UI/PowerButton";
import ParticalComponent from "../../subComponents/ParticalComponent/ParticalComponent";
import astronaut from '../../assets/ImagesDesign/spaceman.png';
import BigTitle from "../../subComponents/BigTitle/BigTitle";

const Box = styled.div`
  background-color: ${props => props.theme.body};
  width: 100vw;
  height: 100vh;
  position: relative;
  overflow: hidden;
  @media screen and (max-width: 450px) {
    overflow: no-content;
  }
`

const float = keyframes`
0% {transform: translateY(-10px)}
  50% {transform: translateY(15px) translateX(15px)}
  100% {transform: translateY(-10px)}
`

const Spaceman = styled.div`
  position: absolute;
  top: 10%;
  right: 5%;
  width: 20vw;
  animation: ${float} 4s ease infinite;
  
  img{
    width: 100%;
    height: auto;
  }
`

const Main = styled.div`
  border: 2px solid ${props => props.theme.text};
  color: ${props => props.theme.text};
  padding: 2rem;
  width: 50vw;
  height: 50vh;
  z-index: 3;
  line-height: 1.5;
  font-size: calc(0.5rem + 0.8vw);
  backdrop-filter: blue(4px);
  
  position: absolute;
  left: calc(5rem + 5vw);
  top: 10rem;
  
  font-family: "Ubuntu mono", monospace;
  font-style: italic;
  a{
    text-decoration: none;
    color: ${props => props.theme.text};
    font-weight: bold;
    font-style: normal;
  }
  @media screen and (max-width: 450px) {
    width: 70vw;
    padding: 0.8rem;
    left: calc(1rem + 2.5vw);
  }
`


const AboutPage = () => {
    return (
        <ThemeProvider theme={darkTheme}>
            <Box>
                <LogoComponent theme="dark"/>
                <PowerButton/>
                <ParticalComponent theme="dark"/>
                <Spaceman>
                    <img src={astronaut} alt="astronaut"/>
                </Spaceman>
                <Main>
                    I am Baiysh and 23 years old. In fact, my University degree was in another space(Environmental engineering), but last University year I understood that my choice was not right. That's why I began to learn coding. And <a
                    href="https://attractor-school.com/bishkek" target="_blank" rel="noreferrer" >Attractor School</a> helped a lot. It was childhood dream. <br/><br/>I'm interested in the whole fullstack like trying new things and building great projects. <br/><br/>My plan to the next 5 years to learn Java, Python languages and to become Senior Fullstack Developer.
                </Main>
                <BigTitle top="75%" right="8%" text="ABOUT"/>
            </Box>
        </ThemeProvider>
    );
};

export default AboutPage;