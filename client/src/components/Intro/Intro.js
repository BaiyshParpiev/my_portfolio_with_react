import React from 'react';
import styled from 'styled-components'
import {motion} from 'framer-motion';

const Box = styled(motion.div)`
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  width: 65vw;
  height:55vh;
  display: flex;
  background: linear-gradient(
          to right,
          ${props => props.theme.body} 50%,
          ${props => props.theme.text} 50%) bottom,
  linear-gradient(
          to right,
          ${props => props.theme.body} 50%,
          ${props => props.theme.text} 50%) top;
  background-repeat: no-repeat;
  background-size: 100% 2px;
  border-left: 2px solid ${props => props.theme.body};
  border-right: 2px solid ${props => props.theme.text};
  z-index:1;
  padding: 30px 40px 55px 40px;
  @media screen and (max-width: 950px){
    background: linear-gradient(
            to top,
            ${props => props.theme.body} 50%,
            ${props => props.theme.text} 50%) left,
    linear-gradient(
            to bottom,
            ${props => props.theme.body} 50%,
            ${props => props.theme.text} 50%) right;
    border-top: 2px solid ${props => props.theme.body};
    border-bottom: 2px solid ${props => props.theme.text};
    width: 50vw;
    height:75vh;
    padding: 30px;
    flex-direction: column;
  }
  @media screen and (max-width: 500px){
    padding: 15px 0 0 0;
    height:50vh;
  }
  @media screen and (max-width: 750px){
    padding-top: 15px;
  }
`

const SubBox = styled.div`
  width: 50%;
  position: relative;
  display: flex;
  .border{
    display: block;
    border-radius: 50%;
    border: 2px solid ${props => props.theme.text};
    width: 90%;
  }
  @media screen and (max-width: 950px){
    width: 100%;
  }
`

const Text = styled.div`
  font-size: calc(1em + 1.5vw);
  color: ${props => props.theme.body};
  padding: 0.5rem;
  cursor: pointer;
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  &>*:last-child {
    color: ${props => `rgba(${props.theme.bodyRgba}, 0.6)`};
    font-size: calc(0.5em + 1.5vw);
    font-weight: 300;
  }
  @media screen and (max-width: 500px){
    margin-top: 10px;
  }
  @media screen and (max-width: 750px){
    h1{
      margin-bottom: 5px;
    }
    h3{
      margin-bottom: 15px;
    }
    margin-bottom: 10px;

  }
  @media screen and (max-width: 950px){
    font-size: calc(0.5em + 1.5vw);
    flex-direction: column;
    justify-content: flex-start;
    width: 100%;
    padding: 15px;
  }
`

const Title = styled.div`
  color: ${props => props.theme.text};
  font-size: calc(1em + 1.5vw);
  font-weight: bold;
  transform: rotate(-35deg);
  margin-top: 4rem;
  margin-left: 0.1rem;
  @media screen and (max-width: 950px){
    transform: rotate(0deg);
    margin-left: 1rem;
    margin-bottom: 1.3rem;
    margin-top: 1rem;
  }
  
  @media screen and (max-width: 750px){
    margin-top: 2rem;
    font-size: calc(0.7em + 0.5vw);
  }
  @media screen and (max-width: 400px){
    margin-top: 4rem;
    font-size: calc(0.6em + 0.5vw);
  }
  @media screen and (max-width: 250px){
    margin-top: 4rem;
    font-size: calc(0.8em + 0.5vw);
  }
  
`

const Intro = () => {
    return (
        <Box
            initial={{height: 0}}
            animate={{
                height: "50vh",
            }}
            transition={{type: 'spring', duration: 2, delay: 1}}
        >
            <SubBox>
                <Text>
                    <h1>Hello,</h1>
                    <h3>I'm Baiysh.</h3>
                    <h6>I make what I enjoy and it is creating simple Websites.</h6>
                </Text>
            </SubBox>
            <SubBox>
                <motion.div
                    initial={{opacity: 0}}
                    animate={{opacity: 1}}
                    transition={{duration: 1, delay: 2}}
                >
                    <Title>
                        <h1>Frontend Developer</h1>
                        <span className="border"/>
                    </Title>
                </motion.div>
            </SubBox>
        </Box>
    );
};

export default Intro;