import React, {useState} from 'react';
import styled, {keyframes} from 'styled-components';
import PowerButton from "../../subComponents/UI/PowerButton";
import LogoComponent from "../../subComponents/UI/LogoComponent/LogoComponent";
import SocialIcons from "../../subComponents/UI/SocialIcons/SocialIcons";
import {NavLink} from "react-router-dom";
import {YinYang} from "../AllSvgs/AllSvgs";
import Intro from "../Intro/Intro";
import {motion} from "framer-motion";
import {darkTheme as DarkTheme} from "../../components/Themes/Themes";


const MainContainer = styled.div`
    background: ${props => props.theme.body};
    width: 100vw;
    height: 100vh;
    overflow: hidden;
    position: relative;
    h2, h3, h4, h5, h6{
      font-family: 'Karla', sans-serif;
      font-weight: 500;
    }
`;

const Container = styled.div`
    padding: 2rem;
`

const Contact = styled(NavLink)`
  color: ${DarkTheme.body};
  position: absolute;
  top: 2rem;
  right: calc(1rem + 2vw);
  text-decoration: none;
  z-index: 1;

  @media screen and (max-width: 950px){
    color: ${props => props.click ? DarkTheme.text : DarkTheme.body}
  }

  @media screen and (max-width: 400px){
    font-size: 14px;
    right: 1rem;
    top: 2.5rem;
  }
`

const Work = styled(NavLink)`
  color: ${props => props.theme.text};
  position: absolute;
  top: 50%;
  transform:rotate(90deg) translate(-50%, -50%);
  right: calc(1rem + 2vw);
  text-decoration: none;
  z-index: 1;
  @media screen and (max-width: 950px){
    color: ${props => props.theme.body};
    top: 45%;
  }
`

const BottomBar = styled.div`
  position: absolute;
  bottom: 1rem;
  left: 0;
  right: 0;
  width: 100%;
  display: flex;
  justify-content: space-evenly;
`

const About = styled(NavLink)`
  color: ${props => props.click ?  props.theme.body : props.theme.text};
  text-decoration: none;
  z-index: 1;
  @media screen and (max-width: 950px){
    color: ${props => props.theme.text};
  }
`

const Skills = styled(NavLink)`
  color: ${props => props.theme.text};
  text-decoration: none;
  z-index: 1;
`

const rotate = keyframes`
    from {
      transform: rotate(0);
    }to{
      transform: rotate(360deg);
     }
`

const Center= styled.div`
  position: absolute; 
  top: ${props => props.click ? '88%' : '50%'};
  left: ${props => props.click ? '88%' : '50%'};
  transform: translate(-50%, -50%);
  border: none;
  outline: none;
  background-color: transparent;
  cursor: pointer;
  
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  transition: all 1s ease;

  &>:first-child{
    animation: ${rotate} infinite 1.5s linear;
  }
  &>:last-child{
    display: ${props => props.click ? 'none' : 'inline-block'};
    padding-top: 1rem;
  }
`

const DarkDiv = styled.div`
  position: absolute;
  background-color: #20232a;
  top: 0;
  bottom: 0;
  right: 50%;;
  width: ${props => props.click ? '50%' : '0%'};
  height: ${props => props.click ? '100%' : '0%'};
  z-index: 1;
  transition: height 0.5s ease, width 1s ease 0.5s;
  @media screen and (max-width: 950px){
    bottom: 50%;
    right: 0;;
    width: ${props => props.click ? '100%' : '0%'};
    height: ${props => props.click ? '50%' : '0%'};
  }
`


const Main = () => {
    const [click, setClick] = useState(false);

    const handleClick = () => {
        setClick(!click)
    }

    return (
        <MainContainer>
            <Container>
                <PowerButton/>
                <LogoComponent theme={click ? 'dark' : 'light'}/>
                <SocialIcons
                    click={click}
                    theme={click ? 'dark' : 'light' }
                />
                <DarkDiv click={click}/>
                <Center click={click}>
                    <YinYang onClick={() => handleClick()} width={click ? 80 : 200} height={click ? 80 : 200} fill="currentColor"/>
                    <span>Click me</span>
                </Center>
                <Contact click={click} theme={click ? 'dark' : 'light' } target="_blank" to={{pathname:"bayish.parpiev@gmail.com"}}>
                    <motion.h2
                        initial={{
                            y:-200,
                            transition: { type:'spring', duration: 1.5, delay:1}
                        }}
                        animate={{
                            y:0,
                            transition: { type:'spring', duration: 1.5, delay:1}
                        }}
                        whileHover={{scale: 1.1}}
                        whileTap={{scale: 0.9}}

                    >
                        Write me..
                    </motion.h2>
                </Contact>
                <Work to="/works" click={+click}>
                    <motion.h2
                        initial={{
                            y:-200,
                            transition: { type:'spring', duration: 1.5, delay:1}
                        }}
                        animate={{
                            y:0,
                            transition: { type:'spring', duration: 1.5, delay:1}
                        }}
                        whileHover={{scale: 1.1}}
                        whileTap={{scale: 0.9}}
                    >
                        Work
                    </motion.h2>
                </Work>
                <BottomBar>
                    <About to="/about" click={+click}>
                        <motion.h2
                            initial={{
                                y:200,
                                transition: { type:'spring', duration: 1.5, delay:1}
                            }}
                            animate={{
                                y:0,
                                transition: { type:'spring', duration: 1.5, delay:1}
                            }}
                            whileHover={{scale: 1.1}}
                            whileTap={{scale: 0.9}}
                        >
                            About.
                        </motion.h2>
                    </About>
                    <Skills to="/skills">
                        <motion.h2
                            initial={{
                                y:200,
                                transition: { type:'spring', duration: 1.5, delay:1}
                            }}
                            animate={{
                                y:0,
                                transition: { type:'spring', duration: 1.5, delay:1}
                            }}
                            whileHover={{scale: 1.1}}
                            whileTap={{scale: 0.9}}
                        >
                            My Skills.
                        </motion.h2>
                    </Skills>
                </BottomBar>
            </Container>
            {click ? <Intro click={click}/> : null}
        </MainContainer>
    );
};

export default Main;