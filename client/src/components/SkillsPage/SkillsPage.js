import React from 'react';
import styled, {ThemeProvider} from "styled-components";
import {lightTheme} from "../Themes/Themes";
import {Design, Develope} from "../AllSvgs/AllSvgs";
import LogoComponent from "../../subComponents/UI/LogoComponent/LogoComponent";
import PowerButton from "../../subComponents/UI/PowerButton";
import ParticalComponent from "../../subComponents/ParticalComponent/ParticalComponent";

const Box = styled.div`
  background-color: ${props => props.theme.body};
  width: 100vw;
  height: 100vh;
  position: relative;
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  @media screen and (max-width: 600px) {
    flex-direction: column;
    height: 100vh;
  }
`

const Main = styled.div`
  border: 2px solid ${props => props.theme.text};
  color: ${props => props.theme.text};
  background-color: ${props => props.theme.body};
  padding: 1.4rem;
  width: 37vw;
  height: 59vh;
  z-index: 3;
  line-height: 1.5;
  cursor: pointer;
  @media screen and (max-width: 600px) {
    margin-top: 3.5rem;
    flex-direction: column;
    height: 32vh;
    width: 80vw;
    padding: 0.5rem;
  }

  font-family: "Ubuntu Mono", monospace;
  display: flex;
  flex-direction: column;
  justify-content: space-between;

  &:hover {
    color: ${props => props.theme.body};
    background-color: ${props => props.theme.text};
  }
`

const Title = styled.h2`
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: calc(0.8em + 1vw);

  ${Main}:hover & {
    color: ${props => props.theme.body};
    background-color: ${props => props.theme.text};
  }
`

const Description = styled.div`
  color: ${props => props.theme.text};
  font-size: calc(0.4em + 1vw);
  padding: 0.5rem 0;

  strong {
    margin-bottom: 1rem;
    text-transform: uppercase;
  }

  ul, p {
    margin-left: 2rem;
  }

  ${Main}:hover & {
    color: ${props => props.theme.body};
    background-color: ${props => props.theme.text};
  }
`

const SkillsPage = () => {
    return (
        <ThemeProvider theme={lightTheme}>
            <Box>
                <LogoComponent theme="light"/>
                <PowerButton/>
                <ParticalComponent theme="light"/>
                <Main>
                    <Title>
                        <Design
                            width={40}
                            height={40}
                            fill="currentColor"
                        />
                        Backend
                    </Title>
                    <Description>
                        I am beginner Developer as you see in this space and made simple fullstack projects.
                    </Description>
                    <Description>
                        <strong>Skills</strong>
                        <p>NodeJs, MySql, mongoDb(mongoose), FireBase, Heroku, expressJs</p>
                    </Description>
                    <Description>
                        <strong>Tools</strong>
                        <ul>
                            <li>DataGrip</li>
                            <li>WebStorm</li>
                            <li>Postman</li>
                        </ul>
                    </Description>
                </Main>
                <Main>
                    <Title>
                        <Develope
                            width={40}
                            height={40}
                            fill="currentColor"
                        />
                        Frontend
                    </Title>
                    <Description>
                        I love to create simple yet beautiful websites with great user experience.
                    </Description>
                    <Description>
                        <strong>Skills</strong>
                        <p>Html, Css, Js, React, Redux, Sass(SCSS), Bootstrap, Jquery, REST(axios, AJAX, fetch), CRUD,
                            @Mui, BEM</p>
                    </Description>
                    <Description>
                        <strong>Tools</strong>
                        <p>VScode, PerfectPixel, AdobePhotoshop, Figma, Linux, Windows, Github, BitBucket
                        </p>
                    </Description>
                </Main>
            </Box>
        </ThemeProvider>
    );
};

export default SkillsPage;