export const lightTheme = {
    body:"#61dafb",
    text:"#20232a",
    fontFamily:"'Source Sans Pro', sans-serif",
    bodyRgba : "97, 218, 251",
    textRgba:"35,35,42",
}
export const darkTheme = {
    body:"#20232a",
    text:"#61dafb",
    fontFamily:"'Source Sans Pro', sans-serif",
    textRgba : "97, 218, 251",
    bodyRgba:"35,35,42",
}

