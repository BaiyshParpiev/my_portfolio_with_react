import React, {useEffect, useState} from 'react';
import styled from 'styled-components';
import img from '../../assets/ImagesDesign/patrick-tomasso-Oaqk7qqNh_c-unsplash.jpg';
import LogoComponent from "../../subComponents/UI/LogoComponent/LogoComponent";
import PowerButton from "../../subComponents/UI/PowerButton";
import SocialIcons from "../../subComponents/UI/SocialIcons/SocialIcons";
import WorkComponents from "../WorkComponents/WorkComponents";
import {WorkData} from "../../data/DataWork/DataWork";
import {motion} from "framer-motion";
import AnchorComponent from "../../subComponents/Anchor/Anchor";
import BigTitle from "../../subComponents/BigTitle/BigTitle";

const MainContainer = styled(motion.div)`
  background-image: url(${img});
  background-size: cover;
  background-repeat: no-repeat;
  background-attachment: fixed;
  background-position: center;
`
const Container = styled.div`
  background-color: ${props => `rgba(${props.theme.bodyRgba},0.8)`};
  width: 100%;
  height:auto;
  position: relative;
  padding-bottom: 5rem;
`

const Center = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding-top: 10rem;
  
`

const Grid = styled.div`
  display: grid;
  grid-template-columns: repeat(2, minmax(calc(10rem + 15vw), 1fr));
  grid-gap: calc(1rem + 2vw);
  @media screen and (max-width: 750px){
    grid-template-columns: repeat(1, minmax(calc(10rem + 10vw), 1fr));
  }
`

const container = {
    hidden: {opacity:0},
    show: {
        opacity:1,

        transition:{
            staggerChildren: 0.5,
            duration: 1,
        }
    }
}

const WorkPage = () => {
    const [numbers, setNumbers] = useState(0);

    useEffect(() => {
        let num = (window.innerHeight - 70)/30;
        setNumbers(parseInt(num));
    }, [])
    return (
        <MainContainer
            variants={container}
            initial='hidden'
            animate='show'
            exit={{
                opacity:0, transition:{duration: 1}
            }}
        >
            <Container>
                <LogoComponent/>
                <PowerButton/>
                <SocialIcons/>
                <AnchorComponent number={numbers}/>
                <Center>
                    <Grid>
                        {WorkData.map(work => (
                            <WorkComponents key={WorkData.indexOf(work)} work={work}/>
                        ))}
                    </Grid>
                </Center>
                <BigTitle text="WORK" top="10%" right="20%" light="light"/>
            </Container>
        </MainContainer>
    );
};

export default WorkPage;