import business from '../../assets/images/business-2.jpg';
import cartpage from '../../assets/images/cart-page.jpg';
import craze from '../../assets/images/craze.jpg';
import cw from '../../assets/images/CW-2.jpg';
import design from '../../assets/images/design-7.jpg';
import designA from '../../assets/images/design-13.jpg';
import flat from '../../assets/images/flat.jpg';
import freedom from '../../assets/images/freedom.jpg';
import icrowd from '../../assets/images/icrowdme.jpg';
import cruise from '../../assets/images/cruises.jpg';
import kappe from '../../assets/images/kappe-homepage.jpg'
import joby from '../../assets/images/joby.jpg'
import intent from '../../assets/images/intent.jpg'
import indipixel from '../../assets/images/indipixel.jpg';
import reform from '../../assets/images/reform-2.jpg';
import form from '../../assets/images/form-desktop.jpg';
import fashion from '../../assets/images/FashionPress.jpg';
import cwPage from '../../assets/images/TheyAllow-desktop.jpg';
import chat from '../../assets/images/Chat.png';
import pokemon from '../../assets/images/pokemon.png';
import todo from '../../assets/images/todoPhoto.png';
import country from '../../assets/images/country.png';
import countries from '../../assets/images/Countries.png';
import orders from '../../assets/images/orders.png';
import poker from '../../assets/images/poker.png';
import todolist from '../../assets/images/todo.png';
import quotes from '../../assets/images/quotes.png';
import burgerApp from '../../assets/images/burger-app.png';
import newsBlog from '../../assets/images/news-blog.png';
import newsComment from '../../assets/images/news-comment.png';
import exPortfolio from '../../assets/images/ex-portfolio.png';


export const WorkData = [
    {image: burgerApp, name: 'Burger app with firebase', link: 'https://burgers-webapp.netlify.app/', tags: ["FullStack", "FireBase"]},
    {image: newsBlog, name: 'It was created with MERN', link: 'https://news-blogs.netlify.app/', tags: ["FullStack", "MERN", "Heroku"]},
    {image: newsComment, name: 'Simple MySql project', link: 'https://github.com/Bayish/new-comment-app', tags: ["FullStack", "mySql"]},
    {image: exPortfolio, name: 'Old portfolio with react', link: 'https://baiysh-ex-portfolio.netlify.app/', tags: ["React", "firebase"]},
    {image: business, name: 'Landing page', link: 'https://bayish.github.io/Business-page/', tags: ["html", "css", "perfectPixel"]},
    {image: cartpage, name: 'Landing page', link: 'https://bayish.github.io/cart-page/', tags: ["html", "css", "perfectPixel"]},
    {image: craze, name: 'Landing page', link: 'https://bayish.github.io/cwPage/', tags: ["html", "css", "perfectPixel"]},
    {image: design, name: 'Landing page', link: 'https://bayish.github.io/design-page/', tags: ["html", "css", "perfectPixel"]},
    {image: flat, name: 'Landing page', link: 'https://bayish.github.io/flat-page/', tags: ["html", "css", "perfectPixel"]},
    {image: designA, name: 'PerfectPixel&DesktopFirst', link: 'https://bayish.github.io/desktopFirstPage/', tags: ["html", "css", "perfectPixel", "responsive"]},
    {image: form, name: 'Landing page', link: 'https://bayish.github.io/Form-page/', tags: ["html", "css", "perfectPixel", "responsive"]},
    {image: icrowd, name: 'Landing page', link: 'https://bayish.github.io/icrowWebPage/', tags: ["html", "css", "perfectPixel"]},
    {image: reform, name: 'Landing page', link: 'https://bayish.github.io/reform-page/', tags: ["html", "css", "perfectPixel"]},
    {image: cw, name: 'Landing page', link: 'https://bayish.github.io/secondWebpage/', tags: ["html", "css", "perfectPixel", "responsive"]},
    {image: cruise, name: 'Landing page', link: 'https://bayish.github.io/cruise/', tags: ["html", "css", "perfectPixel", "responsive"]},
    {image: kappe, name: 'Landing page', link: 'https://bayish.github.io/kappePage/', tags: ["html", "css", "perfectPixel", "animation"]},
    {image: intent, name: 'Landing page', link: 'https://bayish.github.io/intentPage/',  tags: ["html", "css", "responsive", "sass"]},
    {image: indipixel, name: 'Landing page', link: 'https://bayish.github.io/indixpage/',  tags: ["html", "css", "BEM", "sass"]},
    {image: cwPage, name: 'Landing page', link: 'https://bayish.github.io/cw-page/',  tags: ["html", "css", "BEM", "sass"]},
    {image: fashion, name: 'Landing page with Grid', link: 'https://bayish.github.io/fashionPage/', tags: ["html", "css", "responsive", "grid"]},
    {image: todo, name: 'To do app with clear VanillaJs', link: 'https://bayish.github.io/todolistvanilla/',  tags: ["vanillaJS"]},
    {image: freedom, name: 'With framework Bootstrap Landing Page', link: 'https://bayish.github.io/freedom-bootstrap-webpage/'},
    {image: joby, name: 'With framework Bootstrap Landing Page', link: 'https://bayish.github.io/JobyPage/', tags: ["html", "css", "bootstrap", "BEM"]},
    {image: chat, name: 'Chat app', link: 'https://github.com/Bayish/chatSimple', tags: ["ajax", "jquery"]},
    {image: pokemon, name: 'Pokemon list app', link: 'https://bayish.github.io/pokemonInfo/', tags: ["ajax", "jquery"]},
    {image: country, name: 'Country data app', link: 'https://bayish.github.io/country/', tags: ["axios", "jquery"]},
    {image: countries, name: 'Countries data', link: 'https://bayish.github.io/travelPlan/', tags: ["axios", "jquery"]},
    {image: orders, name: 'Order app', link: 'https://github.com/Bayish/orderss', tags: ["react"]},
    {image: poker, name: 'Simple poker game to learn points', link: 'https://github.com/Bayish/poker', tags: ["react"]},
    {image: todolist, name: 'Todo list app with react', link: 'https://github.com/Bayish/TodoList', tags: ["react"]},
    {image: quotes, name: 'Quotes app', link: 'https://github.com/Bayish/Quotes', tags: ["react"]},
];
