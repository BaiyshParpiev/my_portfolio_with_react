import React from 'react';
import styled from 'styled-components';
import './logo.css';
import {darkTheme as DarkTheme} from "../../../components/Themes/Themes";

const Logo = (props) => {
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 50 50"
            fill="#2d545e"
            x={68}
            y={32}
            width={30}
            height={30}
            {...props}
        >
            <path fill={props.fill}
                  d="M1 0C.45 0 0 .45 0 1v16c0 .55.45 1 1 1h7v14H1a1 1 0 00-1 1v16a1 1 0 001 1h16a1 1 0 001-1v-7h14v7c0 .555.445 1 1 1h16c.555 0 1-.445 1-1V33c0-.555-.445-1-1-1h-7V18h7a1 1 0 001-1V1a1 1 0 00-1-1H33a1 1 0 00-1 1v7H18V1c0-.55-.45-1-1-1zm17 10h14v7a1 1 0 001 1h7v14h-7c-.555 0-1 .445-1 1v7H18v-7a1 1 0 00-1-1h-7V18h7c.55 0 1-.45 1-1zM4.594 34h2.812L2 39.406v-2.812zm5 0h2.781c-.027.035-.031.063-.063.094L2 44.406v-2.812zm5 0H16v1.406L3.406 48H2v-1.406zM16 37.656v2.75L8.406 48H5.625c.027-.035.031-.063.063-.094L15.78 37.781c.063-.062.149-.082.219-.125zm0 4.938v2.812L13.406 48h-2.781c.027-.035.031-.063.063-.094z"/>
        </svg>
    )
};

const LogoCom = styled.h1`
    display: inline-block;
    color: ${props => props.color === 'dark' ? DarkTheme.text : DarkTheme.body};
    position: fixed;
    left: 2rem;
    top: 2rem;
    z-index: 3;
`;


const LogoComponent = (props) => {
    return (
        <LogoCom color={props.theme}>
            <div className="App-logo">
                <Logo fill="currentColor"/>
            </div>
        </LogoCom>
    );
};

export default LogoComponent;