import React from 'react';
import {NavLink} from "react-router-dom";
import Gitlab, {BitbucketSvg, Facebook, Github, Linkedin} from "../../../components/AllSvgs/AllSvgs";
import styled from 'styled-components';
import {darkTheme as DarkTheme} from "../../../components/Themes/Themes";
import {motion} from "framer-motion";

const Icons = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  position: fixed;
  bottom: 0;
  left: 2rem;

  z-index: 3;

  & > *:not(:last-child) {
    margin: 0.5rem 0;
  }
  @media screen and (max-width: 320px){
    left: 1rem;
  }
  
  svg{  
    fill: ${props => props.color === 'dark' ? DarkTheme.text : DarkTheme.body};
    @media screen and (max-width: 950px){
      fill: ${DarkTheme.body}
    }
  }
`

const Line = styled(motion.span)`
  width: 2px;
  height: 6rem;
  background-color: ${props => props.color === 'dark' ? DarkTheme.text : DarkTheme.body};
  @media screen and (max-width: 950px){
    background-color: ${DarkTheme.body}
  }
`


const SocialIcons = (props) => {
    return (
        <Icons click={props.click} color={props.theme}>
            <motion.div
                initial={{transform:"scale(0)"}}
                animate={{scale:[0,1,1.5,1]}}
                transition={{type:'spring', duration:1, delay:1}}
            >
                <NavLink to={{pathname: 'https://github.com/Bayish'}} target="_blank" style={{color: 'inherit'}}>
                    <Github  width={25} height={25}/>
                </NavLink>
            </motion.div>
            <motion.div
                initial={{transform:"scale(0)"}}
                animate={{scale:[0,1,1.5,1]}}
                transition={{type:'spring', duration:1, delay:1.2}}
            >
                <NavLink to={{pathname: "https://gitlab.com/bayishparpiev"}} target="_blank" style={{color: 'inherit'}}>
                    <Gitlab
                        width={25}
                        height={25}
                        />
                </NavLink>
            </motion.div>
            <motion.div
                initial={{transform:"scale(0)"}}
                animate={{scale:[0,1,1.5,1]}}
                transition={{type:'spring', duration:1, delay:1.4}}
            >
                <NavLink to={{pathname: 'https://www.facebook.com/baiysh.parpiev'}} target="_blank" style={{color: 'inherit'}}>
                    <Facebook width={25} height={25}/>
                </NavLink>
            </motion.div>
            <motion.div
                initial={{transform:"scale(0)"}}
                animate={{scale:[0,1,1.5,1]}}
                transition={{type:'spring', duration:1, delay:1.6}}
            >
                <NavLink to={{pathname: 'https://bitbucket.org/BaiyshParpiev/'}} target="_blank" style={{color: 'inherit'}}>
                    <BitbucketSvg width={25} height={25}/>
                </NavLink>
            </motion.div>
            <motion.div
                initial={{transform:"scale(0)"}}
                animate={{scale:[0,1,1.5,1]}}
                transition={{type:'spring', duration:1, delay:1.8}}
            >
                <NavLink to={{pathname: 'https://www.linkedin.com/in/bayish-parpiev-0b7534138/'}} target="_blank" style={{color: 'inherit'}}>
                    <Linkedin width={25} height={25}/>
                </NavLink>
            </motion.div>
            <Line click={props.click} color={props.theme}
                  initial={{transform:"scale(0)"}}
                  animate={{scale:[0,1,1.5,1]}}
                  transition={{type:'spring', duration:1, delay:1}}
            />
        </Icons>
    );
};

export default SocialIcons;